//
//  AppReducer.swift
//  Dating App
//
//  Created by Anh Dao on 5/2/16.
//  Copyright © 2016 parth. All rights reserved.
//

import ReSwift
import ReSwiftRouter

struct AppReducer: Reducer {
    
    func handleAction(action: Action, state: State?) -> State {
        switch action {
            case let action as ReSwiftInit:
                return State(
                    navigationState: NavigationState(),
                    routeSpecificState: [:],
                    userState: AuthenticationState(),
                    usersProfileState: UsersProfileState()
                )
            case let action as ActionLoginDone:
                var mutableState = state
                mutableState?.userState.userData = action.user
                return mutableState!
            case let action as SetRouteAction:
                var mutableState = state
                mutableState?.navigationState = NavigationReducer.handleAction(action, state: state?.navigationState);
                return mutableState!
            case let action as ActionGetUsersDone:
                var mutableState = state
                mutableState?.usersProfileState.usersProfile = action.usersProfile
                return mutableState!
            default:
                if (state == nil) {
                    return State(
                        navigationState: NavigationState(),
                        routeSpecificState: [:],
                        userState: AuthenticationState(),
                        usersProfileState: UsersProfileState()
                    )
                } else {
                    return state!;
                }
        }
    }
}

func userStateReducer(state: AuthenticationState?, action: Action) -> AuthenticationState {
    var currentState = state ?? AuthenticationState( userData : UserData() )
    
    switch action {
        case let action as ActionLoginDone:
            currentState.userData = action.user
            break
        case _ as ActionLoginError:
            currentState.userData = nil
        default:
            break
    }
    
    return currentState
}

