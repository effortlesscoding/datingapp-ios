//
//  AppState.swift
//  Dating App
//
//  Created by Anh Dao on 5/2/16.
//  Copyright © 2016 parth. All rights reserved.
//

import Foundation
import ReSwift
import ReSwiftRouter

struct State:StateType, HasNavigationState, HasRouteSpecificState {
    
    var navigationState = NavigationState()
    var routeSpecificState: [String: [String: AnyObject]] = [:]
    var userState  : AuthenticationState
    var usersProfileState : UsersProfileState
}

extension NavigationState {
    
    public func currentNavigation() -> String {
        if (self.route.count > 0) {
            return self.route[self.route.count - 1]
        } else {
            return ""
        }
    }
}

protocol HasRouteSpecificState {
    var routeSpecificState: [String: [String: AnyObject]] { get set }
}

typealias Route = [RouteElementIdentifier]

func routeSpecificKey(route: Route) -> String {
    return route.joinWithSeparator("|")
}
