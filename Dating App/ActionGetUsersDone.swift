//
//  ActionGetUsersDone.swift
//  Dating App
//
//  Created by Kirti Parghi on 5/9/16.
//  Copyright © 2016 parth. All rights reserved.
//

import ReSwift

struct ActionGetUsersDone : Action {
    var usersProfile : [UserProfile]
}
