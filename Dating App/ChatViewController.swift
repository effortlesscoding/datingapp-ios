//
//  ChatViewController.swift
//  Dating App
//
//  Created by Yatin on 04/03/16.
//  Copyright © 2016 Yatin. All rights reserved.
//


import UIKit

class ChatViewController: UIViewController {

    
    
    //@IBOutlet weak var btnMenuIcon: UIBarButtonItem!
   
    //@IBOutlet weak var container: YSLDraggableCardContainer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       // btnMenuIcon.target = self.revealViewController()
        //btnMenuIcon.action = Selector("revealToggle:")
        // self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
  
        let btn1 = UIButton()
        btn1.setImage(UIImage(named: "ic_action_chat"), forState: .Normal)
        btn1.frame = CGRectMake(0, 0, 30, 30)
        ///btn1.addTarget(self, action: Selector("chatIconTapped"), forControlEvents: .TouchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = btn1
        
        let btn2 = UIButton()
        btn2.setImage(UIImage(named: "ic_action_favorite"), forState: .Normal)
        btn2.frame = CGRectMake(0, 0, 30, 30)
        //btn2.addTarget(self, action: Selector("ChoosePersonViewController"), forControlEvents: .TouchUpInside)
        let item2 = UIBarButtonItem()
        item2.customView = btn2
        
        self.navigationItem.rightBarButtonItems = [item1,item2]
        
    }
    
    func chatIconTapped(sender: AnyObject!){
       print("search pressed")
        
    }
   
    func favoriteIconTapped(sender: AnyObject!) {
        print("add pressed")
    }
    
}


