//
//  ChoosePersonViewController.swift
//  SwiftLikedOrNope
//
// Copyright (c) 2014 to present, Richard Burdish @rjburdish
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

import UIKit

class ChoosePersonViewController: UIViewController {
    
}

/*, MDCSwipeToChooseDelegate, ENSideMenuDelegate {
    
    
    @IBOutlet weak var NewView: UIView!
    @IBOutlet weak var btnMenuIcon: UIBarButtonItem!
    var people:[Person] = []
    let ChoosePersonButtonHorizontalPadding:CGFloat = 80.0
    let ChoosePersonButtonVerticalPadding:CGFloat = 20.0
    var currentPerson:Person!
    var frontCardView:ChoosePersonView!
    var backCardView:ChoosePersonView!
    
      var reachability:Reachability?
    
    var fname:String =  ""
    var lname:String = ""
    var agemane:String = ""
    var gender:String = ""
    var profile:String = ""
    var tagline:String = ""
    
    var APIvalue:String = ""
    
  
    
    var array = []
   let  maleArray = NSUserDefaults.standardUserDefaults()

    
  
    override func viewDidAppear(animated: Bool) {  }
    
   
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        
        callAPI()
        
        self.sideMenuController()?.sideMenu?.delegate = self
       
        var navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:  247/255.0, green: 169/255.0, blue: 139/255.0, alpha: 100.0/100.0)
        navBarColor.tintColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1.0)
        navBarColor.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
    }
    
    
    func callAPI()
    {
        var alertController = UIAlertController()
        do {
            let reachability = try Reachability.reachabilityForInternetConnection()
            self.reachability = reachability
            
        } catch{
            print("Unable to create\nReachability with address:")
            return
        }
        
        
        if ((self.reachability!.isReachable()) == true)
            
        {
            
            
            
            LGProgressHUD.showHud(IndeficatorType: LGProgressIndeficatorType.System, view: self.view)
            
            let params = ["SessionID":""] as Dictionary<String, AnyObject>
            
            let mode = maleArray.boolForKey("male")
            if mode == true
            {
                 let strEncoded:String = mainUrl+"nextMaleProfiles"
                APIvalue = "\(strEncoded)"
                  print("male")
            }else
            {
                 let strEncoded:String = mainUrl+"nextFemaleProfiles"
                  APIvalue = "\(strEncoded)"
                  print("female")
            }
         
            
//            let strEncoded:String = mainUrl+"nextMaleProfiles"
            
            let requestOfAPI = request(.GET, APIvalue, parameters: nil, encoding: ParameterEncoding.URL, headers: nil)
            
            requestOfAPI.responseJSON(completionHandler: { (response) -> Void in
                
                print(response.request)
                
                print(" response is :\(response.result)")
                
                print(response.response)
                
                
                
                switch response.result{
                    
                case .Success(let payload):
                    
                    LGProgressHUD.dismiss(self.view)
                    
                    if let x = payload as? Dictionary<String,AnyObject>
                        
                    {
                        print(x)
                        
                        
                        var temp:NSArray = x["profiles"] as! NSArray
                        print(temp)
                        
                        
                        self.array = [temp]
                        
                        print(self.array)
                        
                        var arrcount = self.array.count
                        print(arrcount)
                        
                        
                         for var index = 0; index < 4; ++index {
                            print("index is \(index)")
                            
                            var dict:NSDictionary = temp[index] as! NSDictionary
                            
                            var name = dict["firstName"] as! String
                            var lastName = dict["lastName"] as! String
                            var age = dict["age"] as! Int
                            var gender = dict["gender"] as! String
                            var profilePic = dict["profilePic"] as! String
                            var tagline = dict["tagline"] as! String
                            
                            print(name)
                            self.fname = "\(name)"
                            
                            
                            print(" my name is \(self.fname)")
                            print(lastName)
                            self.lname = "\(lastName)"
                            print(age)
                            self.agemane = "\(age)"
                            print(gender)
                            self.gender = "\(gender)"
                            print(profilePic)
                            self.profile = "\(profilePic)"
                            print(tagline)
                            self.tagline = "\(tagline)"
                            
                            self.defaultPeople()
                        }
                        
                        
                        //let arr:NSArray = dist[""] as! NSArray
                    
                        
                     
                        
                        self.people = self.defaultPeople()
                        
                        // Display the first ChoosePersonView in front. Users can swipe to indicate
                        // whether they like or dislike the person displayed.
                        self.setMyFrontCardView(self.popPersonViewWithFrame(self.frontCardViewFrame())!)
                        self.frontCardView.backgroundColor = UIColor.yellowColor()
                        self.view.addSubview(self.frontCardView)
                        
                        // Display the second ChoosePersonView in back. This view controller uses
                        // the MDCSwipeToChooseDelegate protocol methods to update the front and
                        // back views after each user swipe.
                        self.backCardView = self.popPersonViewWithFrame(self.backCardViewFrame())!
                        self.backCardView.backgroundColor = UIColor.redColor()
                        self.view.insertSubview(self.backCardView, belowSubview: self.frontCardView)
                        
                        // Add buttons to programmatically swipe the view left or right.
                        // See the `nopeFrontCardView` and `likeFrontCardView` methods.
                        //        constructNopeButton()
                        //        constructLikedButton()

                        
                    }
                    
                case .Failure(let error):
                    
                    print(error)
                    
                    LGProgressHUD.dismiss(self.view)
                    
                    
                    alertController = UIAlertController(title: "Alert", message: "Dating App cannot connect to server", preferredStyle: .Alert)
                    let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                    }
                    alertController.addAction(OKAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                    
                    
                    
                    
                }
                
                
                
            })
            
        }
            
        else
            
        {
            alertController = UIAlertController(title: "Alert", message: "The Internet connection appears to be offline", preferredStyle: .Alert)
            let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            }
            alertController.addAction(OKAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            
            
        }

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //self.people = defaultPeople()
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        //self.people = defaultPeople()
        // Here you can init your properties
    }
    func suportedInterfaceOrientations() -> UIInterfaceOrientationMask{
        return UIInterfaceOrientationMask.Portrait
    }
    
    
    // This is called when a user didn't fully swipe left or right.
    func viewDidCancelSwipe(view: UIView) -> Void{
        
        print("You couldn't decide on \(self.currentPerson.Name)");
    }
    
    // This is called then a user swipes the view fully left or right.
    func view(view: UIView, wasChosenWithDirection: MDCSwipeDirection) -> Void{
        
        // MDCSwipeToChooseView shows "NOPE" on swipes to the left,
        // and "LIKED" on swipes to the right.
        if(wasChosenWithDirection == MDCSwipeDirection.Left){
            print("You noped: \(self.currentPerson.Name)")
            
            
        }
        else{
             var alertController = UIAlertController()
            alertController = UIAlertController(title: "", message: "Create a profile to increase your chance of being liked", preferredStyle: .Alert)
            let OkAction = UIAlertAction(title: "Create Profile", style:UIAlertActionStyle.Destructive) { (action) in
                print("yes")
        self.navigationController!.pushViewController(self.storyboard!.instantiateViewControllerWithIdentifier("ProfileStepOneViewController") as! ProfileStepOneViewController, animated: true)

                
            }
            let CancelAction = UIAlertAction(title: "Continue Browsing", style:UIAlertActionStyle.Cancel) { (action) in
                
            }
            
            alertController.addAction(OkAction)
            alertController.addAction(CancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
        // MDCSwipeToChooseView removes the view from the view hierarchy
        // after it is swiped (this behavior can be customized via the
        // MDCSwipeOptions class). Since the front card view is gone, we
        // move the back card to the front, and create a new back card.
        if(self.backCardView != nil){
            self.setMyFrontCardView(self.backCardView)
        }
        
        backCardView = self.popPersonViewWithFrame(self.backCardViewFrame())
        //if(true){
        // Fade the back card into view.
        if(backCardView != nil){
            self.backCardView.alpha = 0.0
            self.view.insertSubview(self.backCardView, belowSubview: self.frontCardView)
            UIView.animateWithDuration(0.5, delay: 0.0, options: .CurveEaseInOut, animations: {
                self.backCardView.alpha = 1.0
                },completion:nil)
        }
    }
    func setMyFrontCardView(frontCardView:ChoosePersonView) -> Void{
        
        // Keep track of the person currently being chosen.
        // Quick and dirty, just for the purposes of this sample app.
        self.frontCardView = frontCardView
        self.currentPerson = frontCardView.person
    }
    
    func defaultPeople() -> [Person]{
        // It would be trivial to download these from a web service
        // as needed, but for the purposes of this sample app we'll
        // simply store them in memory.
        print("my name \(self.fname)")
        print("my age \(self.agemane)")
     
        return[Person(name: "\(self.fname)\(self.lname)", image: UIImage(named: "\(self.profile)"), age: 10, tagLine: " \(tagline)", UniversityName: "\(gender)", sharedFriends: 3, sharedInterest: 3, photos: 5),
            
            Person(name: "\(self.fname)\(self.lname)", image: UIImage(named: "\(self.profile)"), age: 30, tagLine: "\(tagline)", UniversityName: "\(gender)", sharedFriends: 3, sharedInterest: 3, photos: 5),
            Person(name: "\(self.fname)\(self.lname)", image: UIImage(named: "\(self.profile)"), age: 30, tagLine: "\(tagline)", UniversityName: "\(gender)", sharedFriends: 3, sharedInterest: 3, photos: 5),
            Person(name: "\(self.fname)\(self.lname)", image: UIImage(named: "\(self.profile)"), age: 30, tagLine: "\(tagline)", UniversityName: "\(gender)", sharedFriends: 3, sharedInterest: 3, photos: 5),
            Person(name: "\(self.fname)\(self.lname)", image: UIImage(named: "\(self.profile)"), age: 30, tagLine: "\(tagline)", UniversityName: "\(gender)", sharedFriends: 3, sharedInterest: 3, photos: 5),
            Person(name: "\(self.fname)\(self.lname)", image: UIImage(named: "\(self.profile)"), age: 57, tagLine: "\(tagline)", UniversityName: "\(gender)", sharedFriends: 3, sharedInterest: 3, photos: 5)]
       
    }
    
    
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    func sideMenuWillOpen() {
        print("sideMenuWillOpen")
    }
    
    func sideMenuWillClose() {
        print("sideMenuWillClose")
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        print("sideMenuShouldOpenSideMenu")
        return true
    }
    
    func sideMenuDidClose() {
        print("sideMenuDidClose")
    }
    
    func sideMenuDidOpen() {
        print("sideMenuDidOpen")
    }

    func popPersonViewWithFrame(frame:CGRect) -> ChoosePersonView?{
        if(self.people.count == 0){
            return nil;
        }
        
        // UIView+MDCSwipeToChoose and MDCSwipeToChooseView are heavily customizable.
        // Each take an "options" argument. Here, we specify the view controller as
        // a delegate, and provide a custom callback that moves the back card view
        // based on how far the user has panned the front card view.
        let options:MDCSwipeToChooseViewOptions = MDCSwipeToChooseViewOptions()
        options.delegate = self
        //options.threshold = 160.0
        options.onPan = { state -> Void in
            if(self.backCardView != nil){
                let frame:CGRect = self.frontCardViewFrame()
                self.backCardView.frame = CGRectMake(frame.origin.x, frame.origin.y-(state.thresholdRatio * 10.0), CGRectGetWidth(frame), CGRectGetHeight(frame))
            }
        }
        
        // Create a personView with the top person in the people array, then pop
        // that person off the stack.
        
        let personView:ChoosePersonView = ChoosePersonView(frame: frame, person: self.people[0], options: options)
        self.people.removeAtIndex(0)
        return personView
        
    }
    func frontCardViewFrame() -> CGRect{
    
        let horizontalPadding:CGFloat = 20.0
        let topPadding:CGFloat = 60.0
        let bottomPadding:CGFloat = 100.0
        return CGRectMake(horizontalPadding,topPadding,CGRectGetWidth(self.view.frame) - (horizontalPadding * 2), CGRectGetHeight(self.view.frame) - bottomPadding)
    }
    func backCardViewFrame() ->CGRect{
        let frontFrame:CGRect = frontCardViewFrame()
        return CGRectMake(frontFrame.origin.x, frontFrame.origin.y + 10.0, CGRectGetWidth(frontFrame), CGRectGetHeight(frontFrame))
    }
    func constructNopeButton() -> Void{
        let button:UIButton =  UIButton(type: UIButtonType.System)
        let image:UIImage = UIImage(named:"nope")!
        button.frame = CGRectMake(ChoosePersonButtonHorizontalPadding, CGRectGetMaxY(self.frontCardView.frame) + ChoosePersonButtonVerticalPadding, image.size.width, image.size.height)
        button.setImage(image, forState: UIControlState.Normal)
        button.tintColor = UIColor(red: 247.0/255.0, green: 91.0/255.0, blue: 37.0/255.0, alpha: 1.0)
        button.addTarget(self, action: "nopeFrontCardView", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(button)
    }
    
    func constructLikedButton() -> Void{
        let button:UIButton = UIButton(type: UIButtonType.System)
        let image:UIImage = UIImage(named:"liked")!
        button.frame = CGRectMake(CGRectGetMaxX(self.view.frame) - image.size.width - ChoosePersonButtonHorizontalPadding, CGRectGetMaxY(self.frontCardView.frame) + ChoosePersonButtonVerticalPadding, image.size.width, image.size.height)
        button.setImage(image, forState:UIControlState.Normal)
        button.tintColor = UIColor(red: 29.0/255.0, green: 245.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        button.addTarget(self, action: "likeFrontCardView", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(button)
        
    }
    func nopeFrontCardView() -> Void{
        self.frontCardView.mdc_swipe(MDCSwipeDirection.Left)
    }
    func likeFrontCardView() -> Void{
        self.frontCardView.mdc_swipe(MDCSwipeDirection.Right)
    }
    
    @IBAction func conversations(sender: AnyObject) {
        
        let animation : CATransition = CATransition()
        animation.delegate = self
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromRight
        animation.duration = 0.5
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        UIView.beginAnimations(nil, context: nil)
        self.NewView.layer.addAnimation(animation, forKey: kCATransition)
        UIView.commitAnimations()
        NewView.hidden = false
        self.NewView.backgroundColor = UIColor.whiteColor()
        //self.NewView.hidden = true
        self.view.bringSubviewToFront(NewView)

    }
  
   
    @IBAction func backProfile(sender: AnyObject) {
        let animation : CATransition = CATransition()
        animation.delegate = self
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromLeft
        animation.duration = 0.4
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        UIView.beginAnimations(nil, context: nil)
        self.NewView.layer.addAnimation(animation, forKey: kCATransition)
        UIView.commitAnimations()
        NewView.hidden = true

        
    }
       
    
}*/