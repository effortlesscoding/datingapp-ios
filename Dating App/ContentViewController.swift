//
//  ContentViewController.swift
//  Dating App
//
//  Created by Yatin on 07/03/16.
//  Copyright © 2016 Yatin. All rights reserved.
//

import Foundation
import UIKit

class ContentViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    var pageIndex: Int!
    var titleText: String!
    var imageFile: String!
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.imageView.image = UIImage(named: self.imageFile)
        
        
    }
    
    
    
}
