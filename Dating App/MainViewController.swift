//
//  MainViewController.swift
//  Dating App
//
//  Created by Yatin on 01/03/16.
//  Copyright © 2016 Yatin. All rights reserved.
//


import UIKit

class MainViewController: UIViewController, ENSideMenuDelegate  {
    
    
    @IBOutlet weak var btnMenuIcon: UIBarButtonItem!
    //@IBOutlet weak var container: YSLDraggableCardContainer!
  
 
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
         self.sideMenuController()?.sideMenu?.delegate = self
      
      
        
        
        var navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:  247/255.0, green: 169/255.0, blue: 139/255.0, alpha: 100.0/100.0)
        navBarColor.tintColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1.0)
        navBarColor.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
//        btnMenuIcon.target = self.revealViewController()
//        btnMenuIcon.action = Selector("revealToggle:")
       // self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
       
 

    }
        
    
    
    
  
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    // MARK: - ENSideMenu Delegate
    func sideMenuWillOpen() {
        print("sideMenuWillOpen")
    }
    
    func sideMenuWillClose() {
        print("sideMenuWillClose")
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        print("sideMenuShouldOpenSideMenu")
        return true
    }
    
    func sideMenuDidClose() {
        print("sideMenuDidClose")
    }
    
    func sideMenuDidOpen() {
        print("sideMenuDidOpen")
    }

}
