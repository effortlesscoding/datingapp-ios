//
//  MatcherViewController.swift
//  Dating App
//
//  Created by Anh Dao on 5/3/16.
//  Copyright © 2016 parth. All rights reserved.
//

import UIKit
import ReSwiftRouter
import ReSwift

class MatcherViewController: UIViewController {
    static let identifier = "MatcherViewController"
    let store = mainStore
    var getUsersProfileActionCreator = UsersProfileActionCreator()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.store.dispatch(self.getUsersProfileActionCreator.fetchUsersProfile((mainStore.state.userState.userData?.arrProfileToReviewIDs)! as Array<AnyObject>))
    }
    
    override func viewWillAppear(animated: Bool) {
        store.subscribe(self)
    }
}

extension MatcherViewController: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(maybeState: State) {
        
        print(maybeState.usersProfileState.usersProfile?.description)
        // Check if newState has a new user -> go to another view controller
        // Possibly, I should use ReSwiftRouter here...
        print("New state! We have a user or not?")
        if (maybeState.navigationState.currentNavigation() == MatcherViewController.identifier) {
            //continue
        }
    }
}

extension MatcherViewController : Routable {
    
    func pushRouteSegment(
        routeElementIdentifier: RouteElementIdentifier,
        animated: Bool,
        completionHandler: RoutingCompletionHandler) -> Routable {
        completionHandler()
        return self
    }
    
    func popRouteSegment(
        routeElementIdentifier: RouteElementIdentifier,
        animated: Bool,
        completionHandler: RoutingCompletionHandler) {
        dismissViewControllerAnimated(true, completion: completionHandler)
    }
    
    func changeRouteSegment(
        from: RouteElementIdentifier,
        to: RouteElementIdentifier,
        animated: Bool,
        completionHandler: RoutingCompletionHandler) -> Routable {
        completionHandler()
        return self
    }
}
