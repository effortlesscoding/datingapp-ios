//
//  MyMenuTableViewController.swift
//  Dating App
//
//  Created by Yatin on 04/03/16.
//  Copyright © 2016 Yatin. All rights reserved.
//

import Foundation

import UIKit

class MyMenuTableViewController: UITableViewController {
    
    var selectedMenuItem : Int = 0
    var TableArray = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize apperance of table view
        tableView.contentInset = UIEdgeInsetsMake(64.0, 0, 0, 0) //
        tableView.separatorStyle = .None
        tableView.backgroundColor = UIColor.clearColor()
        tableView.scrollsToTop = false
        
        // Preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        tableView.selectRowAtIndexPath(NSIndexPath(forRow: selectedMenuItem, inSection: 0), animated: false, scrollPosition: .Middle)
           TableArray = ["Search Criteria","My Profile","Account","Sign out"]
    }
    
    
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return 4
    
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("CELL")
        
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "CELL")
            cell!.backgroundColor = UIColor.clearColor()
            cell!.textLabel?.textColor = UIColor.darkGrayColor()
            let selectedBackgroundView = UIView(frame: CGRectMake(0, 0, cell!.frame.size.width, cell!.frame.size.height))
            selectedBackgroundView.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.2)
            cell!.selectedBackgroundView = selectedBackgroundView
        }
        
       cell!.textLabel?.text = "\(TableArray[indexPath.row])"
        // cell!.textLabel?.text = "\(TableArray[indexPath.row])"
       
        return cell!
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50.0
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        print("did select row: \(indexPath.row)")
        
        if (indexPath.row == selectedMenuItem) {
            return
        }
        
        selectedMenuItem = indexPath.row
        
        //Present new view controller
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        var destViewController : UIViewController
        switch (indexPath.row) {
        case 0:
            print("Search Criteria")
            
//    self.navigationController!.pushViewController(self.storyboard!.instantiateViewControllerWithIdentifier("SearchCriteriaViewController") as! SearchCriteriaViewController, animated: true)
           // destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SearchCriteria")
            break
        case 1:
            print("My Profile")
//            self.navigationController!.pushViewController(self.storyboard!.instantiateViewControllerWithIdentifier("SearchCriteriaViewController") as! SearchCriteriaViewController, animated: true)
            
            
                        
//            destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SearchCriteriaViewController") as! SearchCriteriaViewController 
            break
        case 2:
            print("Account")
            //destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("Account")
            
            break
        case 3:
            print("Sign out")
            //destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("Sign out")
            
            (UIApplication.sharedApplication().delegate as! AppDelegate).reset()
            
           // destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
            break
            
        default:
       
            //destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("Sign out")
            
            
            break
        }
       // sideMenuController()?.setContentViewController(destViewController)
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
    
}
