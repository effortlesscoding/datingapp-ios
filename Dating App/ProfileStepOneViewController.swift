//
//  ProfileStepOneViewController.swift
//  Dating App
//
//  Created by Yatin on 08/03/16.
//  Copyright © 2016 Yatin. All rights reserved.
//

import Foundation
import UIKit

class ProfileStepOneViewController: UIViewController {
    
 
    
    @IBOutlet weak var txtDescribe: UITextField!
    @IBOutlet weak var txtReligion: UITextField!
    
    @IBOutlet weak var txtDrinking: UITextField!
    
    @IBOutlet weak var txtSmoking: UITextField!
    
    
    @IBOutlet weak var txtGender: UITextField!
    override func viewDidLoad()
    {
        super.viewDidLoad()
         self.title = "Complete your profile"
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.blackColor()]
        var navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:  247/255.0, green: 169/255.0, blue: 139/255.0, alpha: 100.0/100.0)
        navBarColor.tintColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1.0)
        navBarColor.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]

        
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor(red: 244/255, green: 102/255, blue: 117/255, alpha: 1).CGColor
        border.frame = CGRect(x: 0, y: txtReligion.frame.size.height - width, width: txtReligion.frame.size.width, height: txtReligion.frame.size.height)
        border.borderWidth = width
        txtReligion.layer.addSublayer(border)
        txtReligion.layer.masksToBounds = true
        
        let borderDescribe = CALayer()
        
        borderDescribe.borderColor = UIColor(red: 244/255, green: 102/255, blue: 117/255, alpha: 1).CGColor
        borderDescribe.frame = CGRect(x: 0, y: txtDescribe.frame.size.height - width, width: txtDescribe.frame.size.width, height: txtDescribe.frame.size.height)
        borderDescribe.borderWidth = width
        txtDescribe.layer.addSublayer(borderDescribe)
        txtDescribe.layer.masksToBounds = true
        
        let borderDrinking = CALayer()
        //let width = CGFloat(2.0)
        borderDrinking.borderColor = UIColor(red: 244/255, green: 102/255, blue: 117/255, alpha: 1).CGColor
        borderDrinking.frame = CGRect(x: 0, y: txtDrinking.frame.size.height - width, width: txtReligion.frame.size.width, height: txtDrinking.frame.size.height)
        borderDrinking.borderWidth = width
        txtDrinking.layer.addSublayer(borderDrinking)
        txtDrinking.layer.masksToBounds = true
        
        let borderSmoking = CALayer()
        //let width = CGFloat(2.0)
        borderSmoking.borderColor = UIColor(red: 244/255, green: 102/255, blue: 117/255, alpha: 1).CGColor
        borderSmoking.frame = CGRect(x: 0, y: txtSmoking.frame.size.height - width, width: txtSmoking.frame.size.width, height: txtSmoking.frame.size.height)
        borderSmoking.borderWidth = width
        txtSmoking.layer.addSublayer(borderSmoking)
        txtSmoking.layer.masksToBounds = true
        
        
        let borderGender = CALayer()
        //let width = CGFloat(2.0)
        borderGender.borderColor = UIColor(red: 244/255, green: 102/255, blue: 117/255, alpha: 1).CGColor
        borderGender.frame = CGRect(x: 0, y: txtGender.frame.size.height - width, width: txtGender.frame.size.width, height: txtGender.frame.size.height)
        borderGender.borderWidth = width
        txtGender.layer.addSublayer(borderGender)
        txtGender.layer.masksToBounds = true
        
    }
    
    @IBAction func btnGender(sender: AnyObject) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Choose Gender", message: "", preferredStyle: .ActionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        //Create and add first option action
        let maleAction: UIAlertAction = UIAlertAction(title: "Male", style: .Default) { action -> Void in
            print("male")
            self.txtGender.text! = ("Male")
            
        }
        actionSheetController.addAction(maleAction)
        //Create and add a second option action
        let femaleAction: UIAlertAction = UIAlertAction(title: "Female", style: .Default) { action -> Void in
            self.txtGender.text! = ("Female")
            
        }
        actionSheetController.addAction(femaleAction)
        
        //Create and add a second option action
       
        
        
        self.presentViewController(actionSheetController, animated: true, completion: nil)    }
    
    
}
