//
//  ProfileStepTwoViewController.swift
//  Dating App
//
//  Created by Yatin on 08/03/16.
//  Copyright © 2016 Yatin. All rights reserved.
//

import Foundation

import UIKit

class ProfileStepTwoViewController: UIViewController {
    

    @IBOutlet weak var txtHeight: UITextField!
    
    
    @IBOutlet weak var txtOccupation: UITextField!
    
    @IBOutlet weak var txtWork: UITextField!
    
    @IBOutlet weak var txtSchool: UITextField!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor(red: 244/255, green: 102/255, blue: 117/255, alpha: 1).CGColor
        border.frame = CGRect(x: 0, y: txtHeight.frame.size.height - width, width: txtHeight.frame.size.width, height: txtHeight.frame.size.height)
        border.borderWidth = width
        txtHeight.layer.addSublayer(border)
        txtHeight.layer.masksToBounds = true
        
        let borderOccupation = CALayer()
        
        borderOccupation.borderColor = UIColor(red: 244/255, green: 102/255, blue: 117/255, alpha: 1).CGColor
        borderOccupation.frame = CGRect(x: 0, y: txtOccupation.frame.size.height - width, width: txtOccupation.frame.size.width, height: txtOccupation.frame.size.height)
        borderOccupation.borderWidth = width
        txtOccupation.layer.addSublayer(borderOccupation)
        txtOccupation.layer.masksToBounds = true
        
        let borderWork = CALayer()
        //let width = CGFloat(2.0)
        borderWork.borderColor = UIColor(red: 244/255, green: 102/255, blue: 117/255, alpha: 1).CGColor
        borderWork.frame = CGRect(x: 0, y: txtWork.frame.size.height - width, width: txtWork.frame.size.width, height: txtWork.frame.size.height)
        borderWork.borderWidth = width
        txtWork.layer.addSublayer(borderWork)
        txtWork.layer.masksToBounds = true
        
        let borderSchool = CALayer()
        //let width = CGFloat(2.0)
        borderSchool.borderColor = UIColor(red: 244/255, green: 102/255, blue: 117/255, alpha: 1).CGColor
        borderSchool.frame = CGRect(x: 0, y: txtSchool.frame.size.height - width, width: txtSchool.frame.size.width, height: txtSchool.frame.size.height)
        borderSchool.borderWidth = width
        txtSchool.layer.addSublayer(borderSchool)
        txtSchool.layer.masksToBounds = true
        
 
        
    }
    
    
    
}
