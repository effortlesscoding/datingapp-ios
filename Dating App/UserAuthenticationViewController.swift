//
//  ViewController.swift
//  Dating App
//
//  Created by Yatin on 01/03/16.
//  Copyright © 2016 Yatin. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class UserAuthenticationViewController: UIViewController, FBSDKLoginButtonDelegate {

    @IBOutlet weak var fbLogin: FBSDKLoginButton!
    @IBOutlet weak var btnLogin: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.btnLogin.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 3).CGColor
        self.btnLogin.layer.borderWidth = CGFloat(Float(3.0))
        self.btnLogin.layer.cornerRadius = CGFloat(Float(0.0))
        UITextField.appearance().tintColor = UIColor.redColor()
        
        
        
        if(FBSDKAccessToken.currentAccessToken() == nil)
        {
            print("not log in")
        }else
        {
            print("log in")
        }
        
       
        
    }
   
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        if error == nil
        {
            print(FBSDKAccessToken.currentAccessToken().tokenString)
            print("log in complete \(result)")
            
            let loginManager: FBSDKLoginManager = FBSDKLoginManager()
            loginManager.logOut()
            
            
            self.performSegueWithIdentifier("showNew", sender: self)
            
           
            
        }else
        {
            print(error.localizedDescription)
        }
    }
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("user logout")
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
    
    @IBAction func faceLogin(sender: AnyObject) {
        
  

        //let loginButton = FBSDKLoginButton()
        fbLogin.readPermissions = ["public_profile","email","user_friends"]
       // fbLogin.center = self.view.center
        
        fbLogin.delegate = self
        
       // self.view.addSubview(fbLogin)
        

        
    }
//https://developers.facebook.com/docs/facebook-login/ios
/*FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
[login
logInWithReadPermissions: @[@"public_profile"]
fromViewController:self
handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
if (error) {
NSLog(@"Process error");
} else if (result.isCancelled) {
NSLog(@"Cancelled");
} else {
NSLog(@"Logged in");
}
}];*/



}

