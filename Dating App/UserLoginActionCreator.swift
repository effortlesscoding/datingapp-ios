//
//  UserAuthenticationActionCreator.swift
//  Dating App
//
//  Created by Anh Dao on 5/2/16.
//  Copyright © 2016 parth. All rights reserved.
//

import Alamofire
import ReSwift

struct UserLoginActionCreator {
    var _newUser : UserData?
    
    mutating func authenticateUser(accessToken : String) -> Store<State>.AsyncActionCreator {
        return { newState, store, callback in
            LoginService().login(accessToken, callback: { (isSuccess : Bool, user : UserData?) -> Void in
                if (isSuccess && user != nil) {
                    self._newUser = user
                    callback(self.setLoginDone)
                } else {
                    callback(self.setLoginError)
                }
            })
        }
    }
    
    
    func setLoginDone(state: State, store: Store<State>) -> Action? {
        return ActionLoginDone(user : self._newUser)
    }
    
    func setLoginError(state: State, store: Store<State>) -> Action? {
        return ActionLoginError()
    }
    
    
}