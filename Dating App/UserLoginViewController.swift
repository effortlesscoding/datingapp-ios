//
//  UserLoginViewController.swift
//  Dating App
//
//  Created by Anh Dao on 5/2/16.
//  Copyright © 2016 parth. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import ReSwift
import ReSwiftRouter

class UserLoginViewController: UIViewController, FBSDKLoginButtonDelegate {
    static let identifier = "UserLoginViewController"
    let store = mainStore
    var loginActionCreator = UserLoginActionCreator()
    
    @IBOutlet weak var btnFBLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [UIColor.whiteColor().CGColor,UIColor.init(colorLiteralRed: 203.0/255.0, green: 244.0/255.0, blue: 245.0/255.0, alpha: 1.0).CGColor, UIColor.init(colorLiteralRed: 0, green: 200.0/255.0, blue: 207.0/255.0, alpha: 1.0).CGColor]
        self.view.layer.insertSublayer(gradient, atIndex: 1)
        
        btnFBLogin.layer.borderColor = UIColor.whiteColor().CGColor
        btnFBLogin.layer.borderWidth = 3
    }
    
    override func viewWillAppear(animated: Bool) {
        store.subscribe(self)
    }
    
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("user logout")
    }
    
    
    @IBAction func facebookLoginClicked(sender: AnyObject) {
        let login = FBSDKLoginManager.init()
        login.logInWithReadPermissions(["public_profile"], fromViewController: self) { (result, error) in
            
            print(result.token.tokenString)
            
            NSUserDefaults.standardUserDefaults().setValue(result.token.tokenString, forKey: "fbToken")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            if (error != nil) {
                print("\(error)")
                print("Error happened!")
            } else if (result.isCancelled) {
                print("Cancelled!")
            } else {
                self.store.dispatch(self.loginActionCreator.authenticateUser(result.token.tokenString));
            }
        }
    }
}

extension UserLoginViewController: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(maybeState: State) {
        
        print(maybeState.userState.userData)
        // Check if newState has a new user -> go to another view controller
        // Possibly, I should use ReSwiftRouter here...
        print("New state! We have a user or not?")
        if (maybeState.navigationState.currentNavigation() == UserLoginViewController.identifier) {
            if (maybeState.userState.userData != nil && maybeState.userState.userData?._id != 0) {
                self.store.dispatch(
                    SetRouteAction(
                        [UserLoginViewController.identifier, MatcherViewController.identifier]
                    )
                )
            }
        }
    }
}



extension UserLoginViewController: Routable {
    
    
    func pushRouteSegment(
        routeElementIdentifier: RouteElementIdentifier,
        animated: Bool,
        completionHandler: RoutingCompletionHandler) -> Routable {
        completionHandler()
        
        
        if (routeElementIdentifier == MatcherViewController.identifier) {
            let matcherViewController = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewControllerWithIdentifier("MatcherViewController") as! MatcherViewController
            presentViewController(matcherViewController, animated: true,
                                  completion: completionHandler)
            completionHandler()
            return matcherViewController
        }
        return self
    }
    
    func popRouteSegment(
        routeElementIdentifier: RouteElementIdentifier,
        animated: Bool,
        completionHandler: RoutingCompletionHandler) {
        dismissViewControllerAnimated(true, completion: completionHandler)
    }
    
    func changeRouteSegment(
        from: RouteElementIdentifier,
        to: RouteElementIdentifier,
        animated: Bool,
        completionHandler: RoutingCompletionHandler) -> Routable {
        completionHandler()
        return self
    }
    
    
}

    
