//
//  UsersProfileActionCreator.swift
//  Dating App
//
//  Created by Kirti Parghi on 5/8/16.
//  Copyright © 2016 parth. All rights reserved.
//

import Alamofire
import ReSwift

struct UsersProfileActionCreator {
    var usersProfile : [UserProfile]?
    
    mutating func fetchUsersProfile(arrIds : Array<AnyObject>) -> Store<State>.AsyncActionCreator {
        return { newState, store, callback in
            GetUsersService().usersProfile(arrIds, callback: { (isSuccess, usersProfile) in
                if (isSuccess && usersProfile.count > 0) {
                    self.usersProfile = usersProfile
                    callback(self.setGetUsersDone)
                } else {
                    callback(self.setGetUsersError)
                }
            })
        }
    }
    
    func setGetUsersDone(state: State, store: Store<State>) -> Action? {
        return ActionGetUsersDone(usersProfile: self.usersProfile!)
    }
    
    func setGetUsersError(state: State, store: Store<State>) -> Action? {
        return ActionGetUsersError()
    }
}