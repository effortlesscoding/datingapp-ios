//
//  GetUsersService.swift
//  Dating App
//
//  Created by Kirti Parghi on 5/8/16.
//  Copyright © 2016 parth. All rights reserved.
//

import Alamofire
import SwiftyJSON

class GetUsersService {
    func usersProfile(arrIds : Array<AnyObject>, callback : (isSuccess : Bool, usersProfile : [UserProfile]) -> Void ) {
        
        let url = NSURL(string: "\(BASE_URL)user/getUsers")
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(NSUserDefaults.standardUserDefaults().valueForKey("fbToken")!)", forHTTPHeaderField: "Authorization")
                
        let values = [
            "ids" : arrIds
        ]
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        
        // Depending on architecture, this can be a memory leak
        Alamofire.request(request)
            .responseData{ (response) -> Void in
                
                let datastring = NSString(data: response.data!, encoding:NSUTF8StringEncoding)
                print("\(datastring)")
                if let httpError = response.result.error {
                    //let statusCode = httpError.code
                    callback(isSuccess: false, usersProfile : [])
                } else { //no errors
                    let statusCode = (response.response?.statusCode)!
                    if (statusCode == 200) {
                        var err : NSError?
                        let data = JSON(data: response.data!, options: .AllowFragments, error: &err)
                        
                        var usersProfile = [UserProfile]()
                        let arrProfile = data["data"]
                        
                        for indiProfile in arrProfile {
                            
                            let dict = indiProfile.1.dictionaryObject!
                            
                            let userProfile: UserProfile  = UserProfile()
                            userProfile.firstName = dict["profile"]!["firstName"] as! String
                            userProfile.lastName = dict["profile"]!["lastName"] as! String
                            
                            let arrProfileVideo = [ProfileVideo]()
                            
                            for indiProfileVideo in dict["profile"]!["videos"] as! NSArray {
                                let profileVideo : ProfileVideo = ProfileVideo()
                                profileVideo.fileName = indiProfileVideo.valueForKey("fileName") as! String
                                profileVideo.uploaded = indiProfileVideo.valueForKey("uploaded") as! Bool
                                profileVideo.storagePath = indiProfileVideo.valueForKey("storagePath") as! String
                                profileVideo.thumbnail = ""
                            }
                            
                            userProfile.videos = arrProfileVideo
                            
                            usersProfile.append(userProfile)
                        }
                        callback(isSuccess: true,usersProfile: usersProfile)
                        
                    } else {
                        callback(isSuccess: false, usersProfile: [])
                    }
                }
        }
    }
}