//
//  ProfileVideo.swift
//  Dating App
//
//  Created by Anh Dao on 5/2/16.
//  Copyright © 2016 parth. All rights reserved.
//


class ProfileVideo {
    
    var fileName : String
    var thumbnail : String
    var uploaded : Bool
    var storagePath : String
    
    
    init() {
        fileName = ""
        thumbnail = ""
        uploaded = false
        storagePath = ""
    }
}
