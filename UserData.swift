//
//  UserData.swift
//  Dating App
//
//  Created by Anh Dao on 5/2/16.
//  Copyright © 2016 parth. All rights reserved.
//
import Foundation
import SwiftyJSON

class UserData {
    var profile : UserProfile
    var _id : Int64
    var facebookid : Int64
    var profilesToReview : [UserData]
    var profilesMatched : [UserData]
    var arrProfileToReviewIDs : Array<AnyObject>
    
    static func build(rawData : NSData?) -> UserData? {
        if (rawData != nil) {
            var err : NSError?
            let data = JSON(data: rawData!, options: .AllowFragments, error: &err)
            print("\(data)")
            print("\(data["data"])")
            print("\(data["data"]["_id"])")
            print("\(data["data"]["facebookId"])")
            let user = UserData()
            user._id = data["data"]["_id"].int64Value
            user.facebookid = data["data"]["facebookId"].int64Value
            
            print(data["data"]["profilesToReview"])
            
            var arrProfileIds = Array<AnyObject>()
            for dict in data["data"]["profilesToReview"] {
                arrProfileIds.append(dict.1.dictionaryObject!)
            }
            user.arrProfileToReviewIDs = arrProfileIds
            
            return user
        } else {
            return nil;
        }
    }
    
    init() {
        profile = UserProfile()
        _id = 0
        facebookid = 0
        profilesToReview = [UserData]()
        profilesMatched = [UserData]()
        arrProfileToReviewIDs = []
    }
}



