//
//  UserProfile.swift
//  Dating App
//
//  Created by Anh Dao on 5/2/16.
//  Copyright © 2016 parth. All rights reserved.
//


class UserProfile {
    
    var firstName : String
    var lastName : String
    var videos : [ProfileVideo]
    
    init() {
        firstName = ""
        lastName = ""
        videos = [ProfileVideo]()
    }
    
}