//
//  UserState.swift
//  Dating App
//
//  Created by Anh Dao on 5/3/16.
//  Copyright © 2016 parth. All rights reserved.
//

import ReSwift


struct UserState:StateType {
    var userData : UserData
    var conversations : Array<String>
}
